# Frammit

Frammit: A basic Python2.7 &amp; PyGtk application that takes text info data and two sets of ASCII character sequences and outputs the data into an ASCII frame. Each set of the ASCII character sequences are used to define the frame borders that surround the text info data.

## Frammit Output Example

```
/**//**//**//**//**//**//**//**//**//**//**//**//**/
/**//**//**//**//**//**//**//**//**//**//**//**//**/
/**/                                            /**/
/**/  #######################################   /**/
/**/  #                                     #   /**/
/**/  #                                     #   /**/
/**/  #     This is example output from     #   /**/
/**/  #    Frammit! A basic application     #   /**/
/**/  #    written in Python that takes     #   /**/
/**/  #    textual information and ASCII    #   /**/
/**/  #       character sequences and       #   /**/
/**/  #      outputs the data into an       #   /**/
/**/  #            ASCII Frame.             #   /**/
/**/  #                                     #   /**/
/**/  #       Author: Donovan Edwards       #   /**/
/**/  #     Email: robogenus@gmail.com      #   /**/
/**/  #                                     #   /**/
/**/  #                                     #   /**/
/**/  #######################################   /**/
/**/                                            /**/
/**//**//**//**//**//**//**//**//**//**//**//**//**/
/**//**//**//**//**//**//**//**//**//**//**//**//**/
```

## License Policy:
This code in it’s current state is free to download, modify, and use and is licensed as Open-source Software under the following license:

*(Read: LICENSE.md File in this directory tree for licensing details)*

# GNU AGPLv3
**(GNU Affero General Public License v3.0)**

Permissions of this strongest copyleft license are conditioned on making available complete source code of licensed works and modifications, which include larger works using a licensed work, under the same license. Copyright and license notices must be preserved. Contributors provide an express grant of patent rights. When a modified version is used to provide a service over a network, the complete source code of the modified version must be made available.

|**Permissions** | **Conditions** | **Limitations** |
| ----------- | ----------- | ----------- |
| Commercial use | Disclose source | Liability |
| Distribution | License & copyright notice | Warranty |
| Modification | Network use is distribution |         |
| Patent use | Same license |         |
| Private use | State changes |         |

