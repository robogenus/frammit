#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
@author: darcona
CopyRight 2014

Created on Tue Jan 21 17:49:33 2014
"""
import pygtk
pygtk.require('2.0')
import gtk

class WinMenu():
    """
WinMenu Class provides methods and attributes to create an active window menu bar object with menu items specified from the formatted menu_data[] list. The class takes the window object as a parameter when creating an instance. The attributes 'has_accelgroup' and 'menu_data' should be initialized before calling create() method.
    
Format Instructions:
--------------------------------------------------------
menu_data[] values should be a dictionary with key being a menu item such as '_File' the value being a list containing a dict for each sub_menu item. Each dict for a sub_menu item should be formatted in the manner below:
 
{
'subitem':<'sub_nenu_item_text' >,
'type' :<'image'>or<'check'>,
'checked':<True>or<False>
'image':'<pathtoimagefile>or<null',
'acl_txt':'<CONTROL>N'or'null', 
'handler':function_name
}

* If any element of the sub_menu item dict is not needed, then don't include it in the dict
    """
    
    def __init__(self, win):
        
        self.win = win # a reference to container that will contain this 
        self.has_accelgroup = False # whether menu contains any accelgrps              
        self.menu_data = [] # list of all menu items and their params
        self.mb = gtk.MenuBar() # the top level menu bar object
        self.subrefs = [] # list of references to sub-menu objects
        
        
    def create(self):        
        if self.has_accelgroup == True:
            agr = gtk.AccelGroup()
            self.win.add_accel_group(agr)
        else:
            agr = 'null'
            
        for xdict in self.menu_data:
            dlist = xdict.items()
            mitem = dlist[0][0] # the text of menu item i.e. '_File'             
            menu = gtk.MenuItem(mitem) # the top menu item
            submenu = gtk.Menu()    # sub-menu of top menu item
            menu.set_submenu(submenu)  # binding submenu to top menuitem
            sublist = dlist[0][1]
            for ydict in sublist:
                smenuitem = 'null'
                typ = 'null'
                checked = 'null'                   
                img = 'null'
                acl_txt = 'null'                    
                handler = 'null'                
                sitem = ydict.get('subitem')
                if 'type' in ydict:
                    typ = ydict.get('type','null')
                if 'checked' in ydict:
                    checked = ydict.get('checked','null')
                if 'image' in ydict:
                    img = ydict.get('image','null')
                    img = gtk.image_new_from_file(img)
                if 'acl_txt' in ydict:
                    acl_txt = ydict.get('acl_txt', 'null')
                if 'handler' in ydict:
                    handler = ydict.get('handler', 'null')
                if typ == 'image' or typ == 'null':
                    if (img != 'null' and acl_txt != 'null'):
                        smenuitem = gtk.ImageMenuItem(sitem, agr)                        
                        smenuitem.set_image(img)
                        key, mod = gtk.accelerator_parse(acl_txt)
                        smenuitem.add_accelerator("activate", agr, key, 
                            mod, gtk.ACCEL_VISIBLE)                        
                        if handler != 'null':
                            smenuitem.connect("activate", handler)
                        self.subrefs.append(smenuitem)    
                        submenu.append(smenuitem)
                        
                    elif (img == 'null' and acl_txt != 'null'):
                        smenuitem = gtk.ImageMenuItem(sitem, agr)
                        key, mod = gtk.accelerator_parse(acl_txt)
                        smenuitem.add_accelerator("activate", agr, key, 
                            mod, gtk.ACCEL_VISIBLE)                        
                        if handler != 'null':
                            smenuitem.connect("activate", handler)
                        self.subrefs.append(smenuitem)
                        submenu.append(smenuitem)
                        
                    elif (img != 'null' and acl_txt == 'null'):
                        smenuitem = gtk.ImageMenuItem(sitem)
                        smenuitem.set_image(img)                                                
                        if handler != 'null':
                            smenuitem.connect("activate", handler)
                        self.subrefs.append(smenuitem)
                        submenu.append(smenuitem)
                        
                    elif (img == 'null' and acl_txt == 'null'):
                        smenuitem = gtk.ImageMenuItem(sitem)                        
                        if handler != 'null':
                            smenuitem.connect("activate", handler)
                        self.subrefs.append(smenuitem)
                        submenu.append(smenuitem)
                elif typ == 'check':
                    smenuitem = gtk.CheckMenuItem(sitem)
                    if checked != 'null':
                        smenuitem.set_active(checked)
                    else:
                        smenuitem.set_active(False)
                    if handler != 'null':
                        smenuitem.connect("activate", handler)
                    self.subrefs.append(smenuitem)
                    submenu.append(smenuitem)
            
            self.mb.append(menu)
        return self.mb
            
        #vbox = gtk.VBox(False, 2)
        #vbox.pack_start(mb, False, False, 0)
        #self.win.add(vbox)
        
    def getsmi(self, itemnum): # get reference to submenuitem object
        smi = self.subrefs[itemnum]
        return smi
        
    def getallsmi(self): # get all references to all submenuitem objects
        return self.subrefs

    def _info(self): # prints this class' doc string
        print self.__doc__
        
    def _dir(self): # prints this class object's attributes
        print str(dir(self))