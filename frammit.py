#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""

/**//**//**//**//**//**//**//**//**//**//**//**//**/
/**//**//**//**//**//**//**//**//**//**//**//**//**/
/**/                                            /**/
/**/  #######################################   /**/
/**/  #                                     #   /**/
/**/  #                                     #   /**/
/**/  #     This is example output from     #   /**/
/**/  #    Frammit! A basic application     #   /**/
/**/  #    written in Python that takes     #   /**/
/**/  #    textual information and ASCII    #   /**/
/**/  #       character sequences and       #   /**/
/**/  #      outputs the data into an       #   /**/
/**/  #            ASCII Frame.             #   /**/
/**/  #                                     #   /**/
/**/  #       Author: Donovan Edwards       #   /**/
/**/  #     Email: robogenus@gmail.com      #   /**/
/**/  #                                     #   /**/
/**/  #                                     #   /**/
/**/  #######################################   /**/
/**/                                            /**/
/**//**//**//**//**//**//**//**//**//**//**//**//**/
/**//**//**//**//**//**//**//**//**//**//**//**//**/

Created: Sun Jan 19 15:05:02 2014

	###################################
	###################################
	###                             ###
	###*****************************###
	###                             ###
	###     PYTHON ROCKS! :}_~~     ###
	###                             ###
	###*****************************###
	###                             ###
	###################################
	###################################

"""

import pygtk
pygtk.require('2.0')
import gtk, pango
from ui import WinMenu

class PyApp(gtk.Window):
    
    def __init__(self):
        super(PyApp, self).__init__()
        
        self.set_title("Frammit - Text Frame Creator v0.0.1")        
        #self.set_size_request(750, 550)
        self.set_default_size(750, 450)
        #self.set_position(gtk.WIN_POS_CENTER) 
        self.set_resizable(True)  
        
        #self.connect("check-resize", self.on_check_resize)
        
        # Misc Attributes & Configs
        self.width, self.height = self.win_dimensions()
        #print "Window Width = %d" % self.width
        #print "Window Height = %d" % self.height
        
        self.error = "" # assign any error text
        self.framecreated = False # True whn frame is drawn once
        self.fwidth = 0 # text frame total width
        self.seqoffset = 0 # size difference between hchar and vchar
        # a list to store menubar submenu item object refs
        self.smirefs = [] # list of submenu item object references
        self.hmargin = 0 # margin value to draw text from horizontal left or right edge
        self.vmargin = 0 # margin value to draw text from vertical top or bttm edge
        self.fhalign = 'CENTER' # Frame horizontal text alignment
        self.fvalign = 'CENTER' # Frame vertical text alignment
        self.appendmode = False
        self.getnewframe = False
        
        self.dls = str(10)  # default label size 
        self.dlc = '#DDDDDD' # default label color
        self.dlf = 'Liberation Mono' # default label font   
        self.dlstyle = 'Regular' # default label style         
        
        # Labels
        label01 = gtk.Label() # horiz border char sequence label
        label01.set_alignment(0.0,0.5)
        self.mklabel(label01,"Horizontal Border Chars:")
        label01.set_tooltip_text("Char sequence used for\nhorizontal frame border")
        label02 = gtk.Label() # vert border char sequence label
        label02.set_alignment(0.0,0.5)
        self.mklabel(label02,"Vertical Border Chars:")
        label02.set_tooltip_text("Char sequence used for\nvertical frame border")
        label03 = gtk.Label() # frame width entry label
        label03.set_alignment(0.0,0.5)
        self.mklabel(label03,"Frame Width:")
        label03.set_tooltip_text("Enter value for\nframe width size")
        label04 = gtk.Label() # frame height entry label
        label04.set_alignment(0.0,0.5)
        self.mklabel(label04,"Frame Height:")
        label04.set_tooltip_text("Enter value for\nframe height size")
        label05 = gtk.Label() # frame text entry label
        label05.set_alignment(0.0,0.5)
        self.mklabel(label05,"Add Text To Frame:")
        label05.set_tooltip_text("Include text to\nadd to frame")
        
        # Buttons
        self.b01 = gtk.Button("Create New Frame")
        self.b01.set_focus_on_click(False)
        self.b02 = gtk.Button("Clear Text")
        self.b02.set_focus_on_click(False)
        
        self.cb01 = gtk.CheckButton(" _Lock Text ")
        self.cb01.set_focus_on_click(False)        
        self.cb01.set_mode(False) 
        self.cb01.set_tooltip_text("Toggle editable\ntext on or off")
        
        self.cb02 = gtk.CheckButton(" _Wrap Text ")
        self.cb02.set_focus_on_click(False)        
        self.cb02.set_mode(False)
        self.cb02.set_tooltip_text("Toggle text wrap\nmode on or off")
        
        self.cb03 = gtk.CheckButton("Append New")
        self.cb03.set_focus_on_click(False)        
        self.cb03.set_mode(True)
        self.cb03.set_tooltip_text("Will append previous\nframes to textview when\nCreate New button is pressed")
        self.cb03.connect("toggled", self.on_cb03_toggled)
        
        self.b03 = gtk.Button("Copy Frame")
        self.b03.set_focus_on_click(False)
        self.b03.set_tooltip_text("Copy new frame\nto clipboard")
        self.b03.connect("clicked", self.on_b03_clicked)
        
        # Button Event Handlers
        self.b01.connect("clicked", self.on_b01_clicked) # bttn to make frame
        self.b02.connect("clicked", self.on_b02_clicked) # bttn to clear main buffer
        self.cb01.connect("toggled", self.on_cb01_toggled) # checkbutton to toggle locking of main textview edit
        self.cb02.connect("toggled", self.on_cb02_toggled)
        
        # Text Entry        
        self.entry01 = gtk.Entry() # Txt entry for h-border char
        self.entry01.set_max_length(14)
        self.entry01.set_width_chars(16)
        self.entry01.set_text('|==|')
        self.entry01.connect("focus-out-event", self.etry01_focus_out)
        self.entry01.set_tooltip_text("Enter char sequence used for\nhorizontal frame border")
        
        spin01adj = gtk.Adjustment(1,1,28,1,0,0) # ajustment setting for spin01
        self.spin01 = gtk.SpinButton(spin01adj)        
        self.spin01.set_wrap(False)
        self.spin01.set_tooltip_text("Number of times to stack\nchar sequence vertically\nMax is 28")
        self.spin01.connect("value-changed",self.spin01_changed)
        
        self.entry02 = gtk.Entry() # Txt entry for v-border char
        self.entry02.set_max_length(14)
        self.entry02.set_width_chars(16)
        self.entry02.set_text('|/|')
        self.entry02.connect("focus-out-event", self.etry02_focus_out)
        self.entry02.set_tooltip_text("Enter char sequence used for\nvertical frame border")
        
        spin02adj = gtk.Adjustment(1,1,28,1,0,0) # ajustment setting for spin02
        self.spin02 = gtk.SpinButton(spin02adj)
        self.spin02.set_wrap(False)
        self.spin02.set_tooltip_text("Number of times to stack\nchar sequence horizontally\nMax is 28")
        self.spin02.connect("value-changed",self.spin02_changed)
        
        # Getting Frame Width
        spin03adj = gtk.Adjustment(1,1,1024,1,1,0) # ajustment setting for spin03
        self.spin03 = gtk.SpinButton(spin03adj)
        self.spin03.set_wrap(False)
        self.spin03.set_value(32)
        self.spin03.set_tooltip_text("Select value for\nframe width size\nMax: 1024")
        self.spin03.connect("value-changed",self.spin03_changed)
        
        # Getting Frame Height
        spin04adj = gtk.Adjustment(1,1,1024,1,1,0) # ajustment setting for spin04
        self.spin04 = gtk.SpinButton(spin04adj)
        self.spin04.set_wrap(False)
        self.spin04.set_value(18)
        self.spin04.set_tooltip_text("Select value for\nframe height size\nMax: 1024")
        self.spin04.connect("value-changed",self.spin04_changed)
        
        # Main TextView & Buffer Initialization
        self.fontsize = "11"
        self.font = "Monospace"
        fontd = self.font + " " + self.fontsize
        fontdesc = pango.FontDescription(fontd) # setting tv default font to fix width font
        
        self.tv = gtk.TextView() # the main textview widget        
        self.tv.modify_font(fontdesc)
        self.tvbuffer = self.tv.get_buffer() # main textview buffer
        self.tvbufftext = "" # to hold tvbuffer's current text        
        self.tvbuffbkup = ""        
        
        self.tvtagtable = self.tvbuffer.get_tag_table()
        tvtag1 = gtk.TextTag("error")
        tvtag1.set_property("foreground","blue")
        self.tvtagtable.add(tvtag1)
        
        tvscroll = gtk.ScrolledWindow()        
        tvscroll.set_policy(gtk.POLICY_AUTOMATIC, gtk.POLICY_AUTOMATIC)
        tvscroll.add(self.tv)
        
        # Range Sliders
        radj1 = gtk.Adjustment(1,1,1024,0,0,0) # ajustment setting for hslide1
        self.hslide1 = gtk.HScale(radj1)
        self.hslide1.set_size_request(100, 23)
        self.hslide1.set_digits(0)
        self.hslide1.set_value_pos(gtk.POS_RIGHT)
        self.hslide1.set_value(self.spin03.get_value_as_int())
        #self.hslide1.set_round_digits(-1)        
        self.hslide1.set_tooltip_text("Adjust frame width\nMax: 1024")
        self.hslide1.connect("value-changed", self.on_hslide1_vchange)
        self.hslide1.set_can_focus(False)
                
        radj2 = gtk.Adjustment(1,1,1024,0,0,0) # ajustment setting for hslide2
        self.hslide2 = gtk.HScale(radj2)
        self.hslide2.set_size_request(100, 23)
        self.hslide2.set_digits(0)
        self.hslide2.set_value_pos(gtk.POS_RIGHT)
        self.hslide2.set_value(self.spin04.get_value_as_int())
        #self.hslide2.set_round_digits(-1)
        self.hslide2.set_tooltip_text("Adjust frame height\nMax: 1024")
        self.hslide2.connect("value-changed", self.on_hslide2_vchange)
        self.hslide2.set_can_focus(False)
        
        # General Main Window Widgets
        mb = self.get_menu() # creating the main win menubar
        statbar = gtk.Statusbar() # main win status bar
        statbar.set_has_resize_grip(False)        
        
        # Add frame text textview widget
        self.tv2 = gtk.TextView() # minor textview to get text to add to frame
        #self.tv2.set_size_request(120,180)
        self.tv2.modify_font(fontdesc)
        self.tv2buffer = self.tv2.get_buffer() # tv2 buffer
        self.tv2bufftext = "" # tv2 buffers current text
        self.tv2.show()

        tv2scroll = gtk.ScrolledWindow() #tv2 scroll window      
        tv2scroll.set_policy(gtk.POLICY_AUTOMATIC, gtk.POLICY_AUTOMATIC)
        tv2scroll.add(self.tv2)
        tv2scroll.show() 
        
        self.cb_win01 = gtk.CheckButton("Add Text")
        self.cb_win01.set_focus_on_click(False)        
        self.cb_win01.set_mode(False) 
        self.cb_win01.set_tooltip_text("Opens the Add\nText Window")
        self.cb_win01.show()
        self.cb_win01.connect("toggled", self.on_cbwin01_toggled)
        
        btv2 = gtk.Button("Clear Text")
        btv2.set_focus_on_click(False)
        btv2.show()
        btv2.connect("clicked", self.on_btv2_clicked)
        
        btv2hide = gtk.Button("Hide")
        btv2hide.set_focus_on_click(False)
        btv2hide.show()
        btv2hide.connect("clicked", self.on_btv2hide_clicked)
        
        # tv2 frame text alignment comboboxes
        self.cbx1 = gtk.combo_box_new_text()
        self.cbx1.append_text("CENTER")
        self.cbx1.append_text("TOP")
        self.cbx1.append_text("BOTTOM")
        self.cbx1.set_active(0)
        self.cbx1.set_can_focus(False)
        self.cbx1.set_tooltip_text("Select vertical text alignment.\nDefault = Center")        
        self.cbx1.show()
        self.cbx1.connect('changed', self.cbx1_changed)                
        
        cbx1label = gtk.Label() # frame text entry label
        cbx1label.set_alignment(0.0,0.5)
        self.mklabel(cbx1label,"Vertical Alignment:")
        cbx1label.show()
        #cbx1label.set_tooltip_text("Select vertical text\nalignment. Default='Centered'")
        
        cbx1spinadj = gtk.Adjustment(1,0,100,1,0,0)
        self.cbx1spin = gtk.SpinButton(cbx1spinadj)
        self.cbx1spin.set_wrap(True)
        self.cbx1spin.set_tooltip_text("Set margin distance\nfrom text vertical\nalignment choice\nMax is 100")  
        self.cbx1spin.connect("value-changed", self.cbx1spin_changed)
        self.cbx1spin.hide()
        
        self.cbx2 = gtk.combo_box_new_text()
        self.cbx2.append_text("CENTER")
        self.cbx2.append_text("LEFT")
        self.cbx2.append_text("RIGHT")
        self.cbx2.set_active(0)
        self.cbx2.set_can_focus(False)
        self.cbx2.set_tooltip_text("Select horizontal text alignment.\nDefault = Center")        
        self.cbx2.show()
        self.cbx2.connect('changed', self.cbx2_changed)                
        
        cbx2label = gtk.Label() # frame text entry label
        cbx2label.set_alignment(0.0,0.5)
        self.mklabel(cbx2label,"Horizontal Alignment:")
        cbx2label.show()
        #cbx2label.set_tooltip_text("Select vertical text\nalignment. Default='Centered'")
        
        cbx2spinadj = gtk.Adjustment(1,0,50,1,0,0)
        self.cbx2spin = gtk.SpinButton(cbx2spinadj)
        self.cbx2spin.set_wrap(True)
        self.cbx2spin.set_tooltip_text("Set margin distance\nfrom text horizontal\nalignment choice\nMax is 50")
        self.cbx2spin.connect("value-changed", self.cbx2spin_changed)
        self.cbx2spin.hide()
        
        self.win01 = gtk.Window()
        self.win01.set_title("Add Text To Frame")
        #self.win01.set_decorated(False)
        #self.win01.set_type_hint(gtk.gdk.WINDOW_TYPE_HINT_UTILITY)      
        self.win01.set_default_size(230,320)
        self.win01.set_transient_for(self)
        self.win01.set_destroy_with_parent(True) 
        self.win01.set_deletable(False)
        self.win01.set_position(gtk.WIN_POS_CENTER)        
        
        cbx_vbx = gtk.VBox(False,2)
        cbx_hbx1 = gtk.HBox(True,1) 
        cbx_hbx2 = gtk.HBox(True,1)
        cbx_vbx.show()
        cbx_hbx1.show()
        cbx_hbx2.show()
        cbx_vbx.pack_start(cbx1label,False,False,1)
        cbx_vbx.pack_start(cbx_hbx1,False,False,0)
        cbx_vbx.pack_start(cbx2label,False,False,1)
        cbx_vbx.pack_start(cbx_hbx2,False,False,0)
        cbx_hbx1.pack_start(self.cbx1,True,True,0)
        cbx_hbx1.pack_start(self.cbx1spin,True,True,1)
        cbx_hbx2.pack_start(self.cbx2,True,True,0)
        cbx_hbx2.pack_start(self.cbx2spin,True,True,1)
        
        win01vbox = gtk.VBox(False,2)
        win01vbox.set_border_width(4)
        win01hbox = gtk.HBox(True,2)
        win01vbox.show()
        win01hbox.show()
        win01vbox.pack_start(tv2scroll,True,True,3)
        win01vbox.pack_start(cbx_vbx,False,False,3)
        win01vbox.pack_start(win01hbox,False,False,3)
        win01hbox.pack_start(btv2,True,True,0)
        win01hbox.pack_start(btv2hide,True,True,0)
        self.win01.set_resizable(True)
        self.win01.add(win01vbox)
        
        hsep1 = gtk.HSeparator()
        
        # Layouts & Binding
        vbox_root = gtk.VBox(False, 0)
        #hbox_h1 = gtk.HBox(False, 0)        
        vbox_h1_v1 = gtk.VBox(False,5)               
        vbx1 = gtk.VBox(False,5)        
        vbox_entries = gtk.VBox(False,0)
        vbox_tv = gtk.VBox(False,5)
        vbox_tv.set_border_width(8)
        hbox_tv_cntrl = gtk.HBox(False,4)
        self.hpane1 = gtk.HPaned()
        
        frame01label = gtk.Label()
        self.mklabel(frame01label,"   Frame Params   ",9,'Sans','Normal','#A1A1A1')
        frame01 = gtk.Frame()
        frame01.set_label_widget(frame01label)
        frame01.set_shadow_type(gtk.SHADOW_ETCHED_IN)
        
        tbl01 = gtk.Table(15,3,False)
        tbl01.set_row_spacings(3)
        tbl01.set_col_spacings(2)
        # Entry01 Table Pack
        tbl01.attach(label01,1,4,1,2,gtk.FILL,gtk.FILL,0,0)
        tbl01.attach(self.entry01,1,2,2,3,gtk.FILL,gtk.FILL,0,0)
        be1 = gtk.Button("Clear")
        be1.set_focus_on_click(False)
        be1.connect("clicked", self.on_be1_clicked)
        tbl01.attach(be1,2,3,2,3,gtk.FILL,gtk.FILL,0,0)
        tbl01.attach(self.spin01,3,4,2,3,gtk.FILL,gtk.FILL,0,0)
        # Entry02 Table Pack
        tbl01.attach(label02,1,4,4,5,gtk.FILL,gtk.FILL,0,0)
        tbl01.attach(self.entry02,1,2,5,6,gtk.FILL,gtk.FILL,0,0)
        be2 = gtk.Button("Clear")
        be2.set_focus_on_click(False)
        be2.connect("clicked", self.on_be2_clicked)
        tbl01.attach(be2,2,3,5,6,gtk.FILL,gtk.FILL,0,0)
        tbl01.attach(self.spin02,3,4,5,6,gtk.FILL,gtk.FILL,0,0)        
        # Entry03 Table Pack
        tbl01.attach(label03,1,4,7,8,gtk.FILL,gtk.FILL,0,0)
        tbl01.attach(self.spin03,1,2,8,9,gtk.FILL,gtk.FILL,0,0)
        tbl01.attach(self.hslide1,1,4,9,10,gtk.FILL,gtk.FILL,0,0)
        # Entry04 Table Pack
        tbl01.attach(label04,1,4,12,13,gtk.FILL,gtk.FILL,0,0)
        tbl01.attach(self.spin04,1,2,13,14,gtk.FILL,gtk.FILL,0,0)        
        tbl01.attach(self.hslide2,1,4,14,15,gtk.FILL,gtk.FILL,0,0)
        
        # Textview tv2 Pack        
        bcp = gtk.Button("Reset All") # clear all entry fields button
        bcp.set_focus_on_click(False)
        bcp.connect("clicked", self.on_bcp_clicked)
        
        hbx1 = gtk.HBox(True,0)
        hbx2 = gtk.HBox(True,0)
        hbx3 = gtk.HBox(False,0)        
        hbx1.pack_start(self.cb_win01,True,True,1)
        hbx1.pack_start(self.b03,True,True,1)
        hbx2.pack_start(self.cb03,True,True,1)
        hbx2.pack_start(bcp,True,True,1)
        vbx1.pack_start(hbx3,True,True,0)        
        vbx1.pack_start(hbx1,False,False,0)
        vbx1.pack_start(hbx2,False,False,0)
        
        spacer1 = gtk.Label(" ")
        #spacer2 = gtk.Label(" ")        
        
        vbox_entries.pack_start(spacer1,False,False,0)        
        vbox_entries.pack_start(tbl01,False,False,0)
        vbox_entries.pack_start(hsep1,False,False,14)
        vbox_entries.pack_start(vbx1,True,True,0)
        #vbox_entries.pack_start(spacer2,False,False,0)
        
        spacer3 = gtk.Label(" ")
        spacer4 = gtk.Label(" ")
        hbox_entries = gtk.HBox(False,4)
        hbox_entries.pack_start(spacer3,False,False,0)
        hbox_entries.pack_start(vbox_entries,True,True,5)
        hbox_entries.pack_start(spacer4,False,False,0)
        
        frame01.add(hbox_entries)
        vbox_h1_v1.pack_start(frame01,True,True,4)
        vbox_h1_v1.pack_end(self.b01,False,False,1)

        hbox_tv_cntrl.pack_start(self.cb01,False,False,1)  
        hbox_tv_cntrl.pack_start(self.cb02,False,False,1)
        hbox_tv_cntrl.pack_start(self.b02,True,True,1)
        vbox_tv.pack_start(tvscroll,True,True,0)
        vbx2 = gtk.VBox(False,0)
        vbx2.pack_start(hbox_tv_cntrl,False,False,0)
        vbx3 = gtk.VBox(False,0)
        vbx3.pack_start(vbox_tv,True,True,0)
        vbx3.pack_start(vbx2,False,False,0)        
        
        self.hpane1.pack1(vbx3,True,False)
        self.hpane1.pack2(vbox_h1_v1,False,False)
        
        vbox_root.pack_start(mb, False, False, 0)        
        vbox_root.pack_start(self.hpane1,True,True, 5)
        vbox_root.pack_end(statbar,False,False,0)        
        
        # Finalization
        self.add(vbox_root)
        self.set_panedivider()             
        self.connect("destroy", gtk.main_quit)
        self.show_all()
    
    # Signal Event Handler Methods 
    def cbx1_changed(self, cbx):
        self.fvalign = cbx.get_active_text()
        if self.fvalign == 'CENTER':
            self.cbx1spin.hide()
            try:
                self.win01.process_updates(True)
                raise AttributeError()
            except AttributeError as error:
                pass
            if self.framecreated:
                self.get_frame()
            
        elif self.fvalign == 'TOP':
            self.cbx1spin.show()
            try:
                self.win01.process_updates(True)
                raise AttributeError()
            except AttributeError as error:
                pass
            if self.framecreated:
                self.get_frame()
            
        elif self.fvalign == 'BOTTOM':
            self.cbx1spin.show()
            try:
                self.win01.process_updates(True)
                raise AttributeError()
            except AttributeError as error:
                pass
            if self.framecreated:
                self.get_frame()
            
            
    def cbx2_changed(self, cbx):
        self.fhalign = cbx.get_active_text()
        if self.fhalign == 'CENTER':
            self.cbx2spin.hide()
            try:
                self.win01.process_updates(True)
                raise AttributeError()
            except AttributeError as error:
                pass
            if self.framecreated:
                self.get_frame()
            
        elif self.fhalign == 'LEFT':
            self.cbx2spin.show()
            try:
                self.win01.process_updates(True)
                raise AttributeError()
            except AttributeError as error:
                pass
            if self.framecreated:
                self.get_frame()
            
        elif self.fhalign == 'RIGHT':
            self.cbx2spin.show()
            try:
                self.win01.process_updates(True)
                raise AttributeError()
            except AttributeError as error:
                pass
            if self.framecreated:
                self.get_frame()
            
            
    def spin01_changed(self, spinner):
        if self.framecreated:        
            self.get_frame()
        
    def spin02_changed(self, spinner):
        if self.framecreated:
            self.get_frame()
        
    def spin03_changed(self, spinner):
        self.hslide1.set_value(self.spin03.get_value_as_int())
        if self.framecreated:
            self.get_frame()
        
    def spin04_changed(self, spinner):
        self.hslide2.set_value(self.spin04.get_value_as_int())
        self.get_frame()
            
    def cbx1spin_changed(self, spinner):
        if self.framecreated:
            self.get_frame()
        
    def cbx2spin_changed(self, spinner):
        if self.framecreated:
            self.get_frame()
        
    def on_cbwin01_toggled(self, widget):
        if widget.get_active():
            self.win01.show()
        else:
            self.win01.hide()
            
    def etry01_focus_out(self, entry, event):
        # changing h-border char sequence and on change create frame      
        if self.framecreated:
            self.get_frame()
        
    def etry02_focus_out(self, entry, event):
        # changing v-border char sequence and on change create frame      
        if self.framecreated:        
            self.get_frame()
            
    def on_hslide1_vchange(self, slider):
        # getting slider1 update val and updating entry03 val
        val = int(slider.get_value())
        #val = val.rstrip(".")
        self.spin03.set_value(val)
        """
        if self.framecreated:
            self.get_frame()
        """
        
    def on_hslide2_vchange(self, slider):
        # getting slider2 update val and updating entry04 val
        val = int(slider.get_value())
        #val = val.rstrip(".")
        self.spin04.set_value(val)
        """
        if self.framecreated:
            self.get_frame()
        """
            
    def on_btv2hide_clicked(self, widget):
        self.win01.hide()
        self.cb_win01.set_active(False)
    
    def on_b01_clicked(self, widget):        
        self.getnewframe = True
        self.get_frame()
        
    def on_b02_clicked(self, widget):# clear main textview's buffer
        self.tvbuffer.set_text("")
        self.tvbufftext = "" 
        
    def on_b03_clicked(self, widget):
        n = 0;
        
    def on_bcp_clicked(self, widget): # clears all entry fields data
        self.spin01.set_value(1)
        self.spin02.set_value(1)
        self.spin03.set_value(32)
        self.spin04.set_value(18)
        self.entry01.set_text("|==|")
        self.entry02.set_text("|;")
        self.tv2buffer.set_text("")
        self.tv2bufftext = ""
        
    def on_btv2_clicked(self, widget): # clears textview2 buffer
        self.tv2buffer.set_text("")
        self.tv2bufftext = ""        
        
    def on_cb01_toggled(self, widget): # sets editable mode for tv
        if widget.get_active():
            self.tv.set_editable(False)
            print "Textview Lock mode: Locked"
            self.b02.set_sensitive(False)
            self.tv.set_cursor_visible(False)
        else:
            self.tv.set_editable(True)
            print "Textview Lock mode: Unlocked"
            self.b02.set_sensitive(True)
            self.tv.set_cursor_visible(True)
            
    def on_cb02_toggled(self, widget): # sets text wrap mode for tv
        if widget.get_active(): 
            self.tv.set_wrap_mode(gtk.WRAP_WORD_CHAR)
            self.getsmi(3).set_active(True) #setting menucheckitem
            print "Textview text wrap mode: On"
        else:
            self.tv.set_wrap_mode(gtk.WRAP_NONE)
            self.getsmi(3).set_active(False) #setting menucheckitem
            print "Textview text wrap mode: Off"
            
    def on_cb03_toggled(self, checkbox):
        if checkbox.get_active():
            self.appendmode = True            
        else:
            self.appendmode = False
            self.tv2bufftext = ""
            
    def on_be1_clicked(self, widget): # clears horiz border char entry
        self.entry01.set_text("")
        print "Horizontal border char entry field: Cleared"
        
    def on_be2_clicked(self, widget): # clears vert border char entry
        self.entry02.set_text("")
        print "Vertical border char entry field: Cleared"
    
    def on_wrap_text(self, widget): # menuitem sets text wrap mode for tv
        if widget.active: 
            self.tv.set_wrap_mode(gtk.WRAP_WORD_CHAR)
            self.cb02.set_active(True) # updating cb02 to enabled
        else:
            self.tv.set_wrap_mode(gtk.WRAP_NONE)
            self.cb02.set_active(False) # updating cb02 to disabled
    """        
    def on_check_resize(self,event):
        self.dims() 
    """
    
            
    # Support Methods    
    def get_frame(self): # creates and/or updates frame by calling frame create methods
        
        hchar = self.entry01.get_text()   
        vchar = self.entry02.get_text()
        fwidth = self.spin03.get_value_as_int()
        fheight = self.spin04.get_value_as_int()
        
        start = self.tv2buffer.get_start_iter()        
        end = self.tv2buffer.get_end_iter()
        
        tstring = self.tv2buffer.get_text(start,end)
        textlist = tstring.splitlines()
        if len(textlist) > 0:
            self.create_txtframe(hchar,vchar,fwidth,fheight,textlist)           
        else:
             self.create_txtframe(hchar,vchar,fwidth,fheight)
        
        if self.framecreated == False: # sets framecreated to true if first time frame created
            self.framecreated = True 
        
    def tag_text(self, text, tagname, start=None, end=None):
        # This method adds a pre-created tag to selected text in tvbuffer
        self.tvbuffer.set_text(text)
        if start == None:
            start = self.tvbuffer.get_start_iter()
        if end == None:
            end = self.tvbuffer.get_end_iter() 
        tagname = self.tvtagtable.lookup(tagname)
        self.tvbuffer.apply_tag(tagname,start,end)
    
    
    def create_hborder(self,hchar,vchar,fwidth,textlist): 
        """
        Calculates H-border width and size adjustments and creates and 
        returns the H-border char sequence string at the write width
        and height. Also calculates and makes adjustments to the overall
        frame width according to length values of added frame text and
        and updates self.fwidth value, which is global frame width attrib. 
        Returns a string to create_textframe() method.
        """
        hborder = ""
        hcharwidth = len(hchar) # get length of hchar sequence
        vcharwidth = len(vchar)
        seqwidth = 0 # hchar multiplier to achieve aproximate fwidth
        
        if fwidth > (hcharwidth * 2): # making sure fwidth is larger than 2 times hchar sequence                      
            
            if textlist == None: # No text has been entered to be included inside frame
                
                # getting a checking division remainder to determine
                # if h-border char sequence length will divide evenly
                # into the proposed frame width entered by the user
                modulo = fwidth % hcharwidth
                
                # calculating for modulo to determine hwidth adjustment                       
                if modulo > 0: # if true, width of hchar won't divide evenly into fwidth
                    while modulo != 0: #add 1 to fwidth until modulo=0, hchar divides evenly
                        fwidth += 1
                        modulo = fwidth % hcharwidth
                self.fwidth = fwidth # fwidth might be a little larger now
                # seqwidth is a multiplier of how many times to print 
                # hchar sequence to equal self.fwidth
                seqwidth = (fwidth / hcharwidth)
                        
            else: # text has been entered to be included inside frame
                maxln = 0  # will store the length of the longest text string to add to frame              
                cnt = 0
                for txtln in textlist:
                    if cnt == 0: # initial start of loop
                        maxln = len(txtln)
                    else:   # all other after start of loop
                        if len(txtln) > maxln:
                            maxln = len(txtln)
                    cnt += 1
                
                # getting vertical line reference that has the most characters
                # and least amount of spaces but not including the spaces
                # that will be determined later in create_vtextborder() method,
                # 2 times vchar width plus max user text length, this is used
                # to calculate what the max framewidth (self.fwidth) needs to
                # be to compensate for length of vchars and longest text
                vlinewidth = (vcharwidth * 2) + maxln                
                # adding horiz margin size as well
                if self.fhalign == "LEFT" or self.fhalign == "RIGHT":
                    #vlinewidth = (self.hmargin * 2) + vlinewidth 
                    vlinewidth = self.hmargin + vlinewidth
                
                # if text length less than fwidth calculate difference
                # and if if diff is odd number add 1 to make even.
                if vlinewidth <= fwidth:
                    wdiff = fwidth - vlinewidth
                    t_modulo = wdiff % 2
                    if t_modulo > 0:
                        fwidth += 1                                                      
                else:                    
                    # Here in the following code we adjusts fwidth to compensate 
                    # for the text being larger than fwidth adding the difference
                    # to fwidth and converting to an even number
                    wdiff = vlinewidth - fwidth                    
                    fwidth += wdiff
                    t_modulo = wdiff % 2
                    if t_modulo > 0:
                        fwidth += 1
                            
                # checking for fwidth to be even again
                modulo = fwidth % hcharwidth
                
                # adding 1 to fwidth until hchars width will divide 
                # evenly into fwidth, thus h-borders total char count
                # becoming equal to fwidth                     
                if modulo > 0: 
                    while modulo != 0: #add 1 to fwidth until modulo=0
                        fwidth += 1
                        modulo = fwidth % hcharwidth
                
                self.fwidth = fwidth
                # sequence width --> the hchar multiplyer
                seqwidth = (fwidth / hcharwidth)
            
            # value of num times to duplicate hchar sequence vertically
            # making the horizontal top and bottom borders thicker
            spnval = self.spin01.get_value_as_int() # border thickness multiplyer
            if spnval == 1:
                hborder = (hchar) * seqwidth # only 1 line thick
            else:                                           
                for cnt in range(spnval):
                    if cnt == (spnval - 1):                                   
                        hborder += ((hchar) * seqwidth)
                                                
                    else:
                        hborder += ((hchar) * seqwidth) + "\n"                        
                        
            return hborder
        
        else:
            try: # Frame width value error, passing error message to be displayed
                raise ValueError(
                """
Input Value Error: Frame cannot be created.

Solution:
Horizontal border width to horizontal border
char sequence size ratio is to low. Please
enter a larger value for horizontal border
width or reduce the number of chars in the
H-border char sequence. The H-border width
value should be more than twice the number
of H-border characters.
                """)
            except ValueError as error:
                self.error = error.message
            
            return None  # returning hborder as None. Frame creation failed
        
    def create_vborder(self,vchar): 
        """
        Creating and calculating vertical chars width size and returning
        a string as vborder to create_textframe() method.
        """                  
        vcharwidth = len(vchar) # get length of vchar sequence
        padding = (self.fwidth - (vcharwidth * 2))
        spacer = " "
        spacing = spacer * padding
        vborder = vchar+spacing+vchar
        return vborder
        
    def create_vtextborder(self,vchar,textlist):
        """
        Creating and calculating vertical border size and dimensions
        with added frame text, if frame text exists, and returning as 
        a list of strings to loop over in the create_textframe() method.
        Also, calculating horizontal alignments: center,left,right.
        """
        vtextborder = []
        self.vmargin = self.cbx1spin.get_value_as_int()
        self.hmargin = self.cbx2spin.get_value_as_int()        
        vcharwidth = len(vchar)
        innerwidth = (self.fwidth - (vcharwidth * 2)) 
        thlength = 0
        pre = ''
        pst = ''
        spacer = " "
        if textlist != None:
            for txt in textlist:
                txtsize = len(txt)                
                if self.fhalign == 'CENTER': # if alignment selected is center - this is default
                    head = 0
                    tail = 0
                    thlength = self.fwidth - ((vcharwidth*2)+txtsize)
                    modulo = thlength % 2
                    if modulo > 0:
                        thlength += 1
                        head = (thlength / 2) - 1
                        tail = thlength - (head+1)
                        
                    else:
                        head = (thlength / 2)
                        tail = thlength - head
                        
                    
                    pre = vchar+((spacer)*head)
                    pst = (spacer*tail) + vchar                    
                    vtextborder.append(pre + txt + pst)
                    
                elif self.fhalign == 'LEFT': # if alignment selected is left
                    tail = (innerwidth - (self.hmargin + txtsize))
                    pre = vchar+((spacer)*self.hmargin)
                    pst = (spacer * tail) + vchar
                    vtextborder.append(pre + txt + pst)
                    
                elif self.fhalign == 'RIGHT': # if alignment selected is right
                    head = (innerwidth - (self.hmargin + txtsize))
                    pre = vchar + (spacer * head)
                    pst = ((spacer)*self.hmargin) + vchar
                    vtextborder.append(pre + txt + pst)
                    
            return vtextborder
        else:
            return None
    
    def create_txtframe(self,hchar,vchar,fwidth,fheight,textlist=None):        
        """
        Main frame creation method. Creates the char box frame by calling 
        support creation methods create_hborder, create_vborder, and 
        create_vtextborder. For all callbacks that call for frame creation
        or update call self.get_frame() method, which calls this method.
        """ 
        self.fvalign = self.cbx1.get_active_text()
        self.vmargin = self.cbx1spin.get_value_as_int()
        hspnval = self.spin01.get_value_as_int()        
        vspnval = self.spin02.get_value_as_int()
        if vspnval > 1:
            vchar = (vchar) * vspnval
        framelines = [] # list to hold all str lines that create the frame
        self.seqoffset = len(hchar) - len(vchar)        
        hborder = self.create_hborder(hchar,vchar,fwidth,textlist) #horiz border
        vborder = self.create_vborder(vchar) #vert border
        vtextborder = []
        vbrdheadseq = 0
        vbrdtailseq = 0
        numtextlines = 0
        
        if hborder != None:
            if textlist != None: #calling methods and populating
                vtextborder = self.create_vtextborder(vchar,textlist) # getting frame text v-border lines
                numtextlines = len(textlist) # numbr of textlist lines of text used to calculate v-border height        
                tmpint = fheight  - (numtextlines + (hspnval*2)) # num lines of v-border text lines + top,bottom hchar size                
                if self.fvalign == "CENTER":
                    modulo = tmpint % 2
                    if modulo > 0:
                        tmpint += 1
                        vbrdheadseq = (tmpint / 2) - 1
                        vbrdtailseq = vbrdheadseq + 1
                        
                    else:
                        vbrdheadseq = tmpint / 2
                        vbrdtailseq = vbrdheadseq  
                        
                elif self.fvalign == "TOP":
                    vbrdheadseq = self.vmargin
                    vbrdtailseq = tmpint - self.vmargin
                    """
                    modulo = tmpint % 2                    
                    if modulo > 0:
                        tmpint += 1                        
                        vbrdheadseq = self.vmargin
                        vbrdtailseq = tmpint - self.vmargin                        
                        
                    else:
                        vbrdheadseq = self.vmargin
                        vbrdtailseq = tmpint - self.vmargin
                    """
                        
                elif self.fvalign == "BOTTOM":
                    vbrdheadseq = tmpint - self.vmargin 
                    vbrdtailseq = self.vmargin
                    """
                    modulo = tmpint % 2
                    if modulo > 0:
                        tmpint += 1                        
                        vbrdheadseq = tmpint - self.vmargin 
                        vbrdtailseq = self.vmargin                       
                        
                    else:
                        vbrdheadseq = tmpint - self.vmargin 
                        vbrdtailseq = self.vmargin
                    """
                        
            """
            called all border build methods and now populating
            the framelines list to be able to loop over each frame 
            line and add it to the long framestring variable which
            will be sent to self.tvbuffer to display in main tv
            textview widget.
            """            
            framelines.append(hborder) # adding top horizontal border
            if textlist == None: # no user text to add, go ahead create frame without text              
                for cnt in range(fheight - 2): # adding vertical border lines minus both hborders
                    framelines.append(vborder)
                framelines.append(hborder) # adding bottom closing h-border               
            else:                # user added text to frame, create frame with text
                for cnt in range(vbrdheadseq):
                    framelines.append(vborder)                   
                    
                for vfline in vtextborder:
                    framelines.append(vfline)                    
                
                for cnt in range(vbrdtailseq):
                    framelines.append(vborder)                    
                    
                framelines.append(hborder)            
            
            # building the full frame print string here to send to textview_buffer
            framestring = "0"        
            for line in framelines:
                if framestring == "0":                
                    framestring = line + "\n"
                else:
                    framestring += line + "\n"
                    
            if self.appendmode:
                if self.getnewframe:
                    start = self.tvbuffer.get_start_iter()        
                    end = self.tvbuffer.get_end_iter()
                    self.tvbufftext = "\n" + self.tvbuffer.get_text(start,end)
                    self.getnewframe = False
                appndstring = self.tvbufftext
                framestring += appndstring
                self.tvbuffer.set_text(framestring)
                
            else:
                self.tvbuffer.set_text(framestring)
                
        else:
            self.tag_text(self.error,'error')
            self.error = ""            
                
    
    def win_dimensions(self): # return the width and height of main window
        width, height = self.get_size()
        return (width, height)
    
    def set_panedivider(self): # set the hpane1 divider position
        width, height = self.win_dimensions()
        divpos = int((width / 3) * 2)
        self.hpane1.set_position(divpos)
    
    def mklabel(self, label, lbltxt, size=None, font=None, style=None, color=None, extra=None): 
    # Default label markup gen
        if extra == None:
            extra = 'null'
        if size == None:
            size = self.dls
        else:
            size = str(size)
        if font == None:
            font = self.dlf
        if style == None:
            style = self.dlstyle
        if color == None:
            color = self.dlc
            
        space = ' '
        
        # default font_desc markup
        dlfd = font+space+style+space+size
        
        # pre markup tag text
        if extra != 'null':
            pre_markup = "<span font_desc='"+dlfd+"' foreground='"+color+"' "+extra+">"
        else:
            pre_markup = "<span font_desc='"+dlfd+"' foreground='"+color+"'>"
            
        # post markup tag text
        pst_markup = "</span>"        
        
        # the full markup tag string
        lbl_str = pre_markup + lbltxt + pst_markup        
        label.set_markup(lbl_str)
        
    def get_menu(self):        
        menudata = [{'_File':[
        {'subitem':'_New',
        'image':"/home/darcona/Icons/newfile_icon16.png",
        'acl_txt':"<CONTROL>N"},
        {'subitem':'_Open'},
        {'subitem':'_Quit',
         'acl_txt':"<CONTROL>Q",
         'handler':gtk.main_quit}
         ]},
         {'_Edit':[
         {'subitem':'_Wrap Text',
          'type':'check',
          'handler':self.on_wrap_text}
         ]}]        

        menubar = WinMenu(self)
        menubar.has_accelgroup = True
        menubar.menu_data = menudata
        mb =  menubar.create()
        
        self.smirefs = menubar.getallsmi()        
        #menubar._info()
        return mb
    
    def getsmi(self, indexnum): # get submenuitem object reference
        return self.smirefs[indexnum]
    
    # Debugging Methods !TO BE REMOVED BEFORE PUBLISHING        
    def dims(self):
        self.width, self.height = self.win_dimensions()
        txt2prnt = "\nWindow Width = %d" % self.width
        txt2prnt = txt2prnt + "\nWindow Height = %d\n" % self.height                
        self.tag_text(txt2prnt,"error")
        
    def debug(self, txt2prnt, tvout=None):        
        if tvout == None:
            print txt2prnt
        elif tvout == 1:
            start = self.tvbuffer.get_start_iter()
            end = self.tvbuffer.get_end_iter()
            text = self.tvbuffer.get_text(start,end)
            self.tvbuffer.set_text(text+txt2prnt)
    
PyApp()
gtk.main()
